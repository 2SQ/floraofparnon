#!/usr/bin/env bash

# Abort if anything fails
set -e

#-------------------------- Settings --------------------------------

# $PROJECT_ROOT is passed from dsh
DOCROOT='docroot'
DOCROOT_PATH=$PROJECT_ROOT/$DOCROOT
# Set to the appropriate site directory
SITE_DIRECTORY='default'
SITEDIR_PATH="${DOCROOT_PATH}/sites/${SITE_DIRECTORY}"
# Set to the appropriate site domain
SITE_DOMAIN='floraofparnon.drude'
# Set to the appropriate website URL
LIVE_SITE_DOMAIN='https://floraofparnon.2square.gr'
# Database export-import file (must be in a versioned directory)
SOURCE_DBEXPORT="$PROJECT_ROOT/.drude/drupal_bricks/DB.sql"
# Select local or prod environment
ENV='prod'

#-------------------------- END: Settings --------------------------------


#-------------------------- Helper functions --------------------------------

# Console colors
red='\033[0;31m'
green='\033[0;32m'
yellow='\033[1;33m'
NC='\033[0m'

echo-red () { echo -e "${red}$1${NC}"; }
echo-green () { echo -e "${green}$1${NC}"; }
echo-yellow () { echo -e "${yellow}$1${NC}"; }

if_failed ()
{
	if [ ! $? -eq 0 ]; then
		if [[ "$1" == "" ]]; then msg="an error occured"; else msg="$1"; fi
		echo-red "$msg";
		exit 1;
	fi
}

# Copy a settings file.
# Skips if the destination file already exists.
# @param $1 source file
# @param $2 destination file
copy_settings_file()
{
    local source=${1}
    local dest=${2}

    if [[ ! -f $dest ]]; then
        echo-green "Copying ${dest}..."
        cp $source $dest
    else
        echo-yellow "${dest} already in place - replacing"
				rm ${dest}
				cp $source $dest
    fi
}

#-------------------------- END: Helper functions --------------------------------


#-------------------------- Functions --------------------------------

# Initialize local settings files
init_settings ()
{
    echo-green "Initializing local project configuration..."
    # Copy from settings templates
    copy_settings_file "${PROJECT_ROOT}/.drude/drude-${ENV}.yml" "${PROJECT_ROOT}/docker-compose.yml"


}

# Set file/folder permissions
file_permissions ()
{
    echo-green "Setting file/folder permissions..."
    mkdir -p $DOCROOT_PATH/sites/$SITE_DIRECTORY/files
    chmod 777 $DOCROOT_PATH/sites/$SITE_DIRECTORY/files
		chmod 777 $DOCROOT_PATH/vendor/
}

# Install site
site_install ()
{
    echo-green "Installing site..."
    cd $DOCROOT_PATH && \
    dsh exec drush8 si -y
}


# Recreate site
site_recreate ()
{
    echo-green "Recreating site..."
    cd $PROJECT_ROOT && \
		echo -e "${green}Building drupal installation with composer ..."
		dsh up bash
	#	dsh exec composer create-project drupal/drupal docroot --stability stable --no-interaction --verbose
		# Copy composer.json file
  #  copy_settings_file "${PROJECT_ROOT}/.drude/drupal_bricks/composer.json" "${DOCROOT_PATH}/composer.json"
	#	cd $DOCROOT_PATH && \
		#dsh exec composer install --no-interaction --verbose

}

# Copy drupal settings
settings_copy ()
{
    echo-green "Copying drupal settings files..."
		chmod 777 $DOCROOT_PATH/sites/$SITE_DIRECTORY/
	#	copy_settings_file "${PROJECT_ROOT}/.drude/drupal_bricks/default.settings.php" "${SITEDIR_PATH}/settings.php"
		copy_settings_file "${PROJECT_ROOT}/.drude/drupal_bricks/example.settings.local.php" "${SITEDIR_PATH}/settings.local.php"
		cd $PROJECT_ROOT && \
		chmod 777 $DOCROOT_PATH
}

# Import database from the source site alias
db_import ()
{
  #_confirm "Do you want to import DB from ${SOURCE_DBEXPORT}?"

  cd $DOCROOT_PATH && \
  dsh mysql-import ${SOURCE_DBEXPORT}
}

# Cleanup tasks
cleanup ()
{
	cd $DOCROOT_PATH && \
	dsh exec drush cr
}
#-------------------------- END: Functions --------------------------------


#-------------------------- Execution --------------------------------

init_settings

site_recreate
file_permissions
settings_copy
dsh reset
# Give MySQL some time to start
echo-green "Waiting 10s for MySQL to start..."; sleep 10

time db_import
cleanup

echo-green "Add ${SITE_DOMAIN} to your hosts file (/etc/hosts), e.g.:"
echo-green "192.168.10.10  ${SITE_DOMAIN}"
echo-green "Alternatively configure wildcard DNS resolution and never edit the hosts file again! Instructions:"
echo-green "https://github.com/blinkreaction/boot2docker-vagrant#dns"
echo-green "Open http://${SITE_DOMAIN} in your browser to verify the setup."

#-------------------------- END: Execution --------------------------------
